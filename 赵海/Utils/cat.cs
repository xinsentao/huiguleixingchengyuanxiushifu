﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    internal
       class Cat
    {
        public string Nickname { get; set; }

        private int age;

        public void Set(int value) 
        {
            if (value > 0 && value < 120)
            {
                age = value;
            }
            else
            {
                age = 0;
            }
        }

        public int Get()
        {
            return age; 
        }
    }
}
